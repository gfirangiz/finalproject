import Components.MainComponent;
import Exceptions.Driver.DriverFactory;
import Exceptions.DriverNotSupportedException;
import Pages.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MainPageTests {
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkMainTitle() {
        new MainPage(driver)
                .open("");
        new MainComponent(driver)
                .pageTitleShouldBeSameAs("ABB - Müasir, Faydalı, Universal");
    }

    @Test
    public void checkCardsTitle() {
        new MainPage(driver)
                .open("");
        new MainComponent(driver)
                .chooseCardsPage()
                .pageTitleShouldBeSameAs("Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları");
    }
    @Test
    public void checkSelectedCardsName() {
        new MainPage(driver)
                .open("");
        CardsPage cardsPage = new MainComponent(driver).chooseCardsPage();
        cardsPage.verifyPage("https://abb-bank.az/az/ferdi/kartlar");
        cardsPage.selectedCardPage().pageTitleShouldBeSameAs("TamKart MasterCard Debet");
    }
    @Test
    public void checkMobileBank() {
        new MainPage(driver)
                .open("");
        Bank24_7_Page bank24_7_page = new MainComponent(driver).chooseBank24();
        bank24_7_page.verifyPage("https://abb-bank.az/az/ferdi/bank-24-7");
        bank24_7_page.abbMobileBankPage().pageTitleShouldBeSameAs("ABB mobile – Sadə və sürətli");
    }
    @Test
    public void checkMortgage() {
        new MainPage(driver)
                .open("");
        MortgagePage mortgagePage = new MainComponent(driver).chooseMortgage();
        mortgagePage.verifyPage("https://abb-bank.az/az/ferdi/kreditler/ipoteka-kreditleri");
        InternalMortgagePage internalMortgagePage = mortgagePage.internalMortgagePage();

        internalMortgagePage.formNoButton.click();
        internalMortgagePage.checkRadioButton(true);
    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}

package Exceptions.Driver.impl;

import Exceptions.DriverNotSupportedException;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class EdgeWebDriver implements IDriver{
    @Override
    public WebDriver newDriver() throws DriverNotSupportedException {
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.addArguments("--no-sandbox");
        edgeOptions.addArguments("--no-first-run");
        edgeOptions.addArguments("--homepage=about:blank");
        edgeOptions.addArguments("--ignore-certificate-errors");
        downloadLocalWebDriver(DriverManagerType.EDGE);

        return new EdgeDriver(edgeOptions);
    }
}

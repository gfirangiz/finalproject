package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.time.Duration;

public abstract class AbsPageObject {
    protected WebDriver driver;
    protected Actions actions;

    public AbsPageObject(WebDriver driver) {
        this.driver = driver;
        this.actions = new Actions(driver);
        //this.driver.manage().window().maximize();
        this.driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        PageFactory.initElements(driver, this);
    }

    public void pageTitleShouldBeSameAs(String title) {
        Assert.assertEquals(
                title,
                this.driver.getTitle(),
                String.format("Title should be %s", title));
    }
    public void verifyPage(String url){
        Assert.assertEquals(url,
                this.driver.getCurrentUrl(),
                String.format("Url should be %s", url));
    }

}

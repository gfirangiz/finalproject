package Components;

import PageObject.AbsPageObject;
import org.openqa.selenium.WebDriver;

public abstract class AbsBaseComponent extends AbsPageObject {
    public AbsBaseComponent(WebDriver driver) {
        super(driver);
    }
}

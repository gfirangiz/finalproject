package Components;

import Pages.Bank24_7_Page;
import Pages.CardsPage;
import Pages.MortgagePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainComponent extends AbsBaseComponent {
    public MainComponent(WebDriver driver) {
        super(driver);
    }
    @FindBy (css = "#js-header-s3 > li.pr-4 > span > a[href=\"https://abb-bank.az/az/ferdi/kartlar\"]")
    private WebElement cards;

    public CardsPage chooseCardsPage(){
        cards.click();
        return new CardsPage(driver);
    }

    @FindBy (css = "#js-header-s3 > li.pr-4 > span > a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement bank24;

    public Bank24_7_Page chooseBank24(){
        bank24.click();
        return new Bank24_7_Page(driver);
    }

    @FindBy (css = "#js-header-s3 > li.pr-4 > span > a[href=\"https://abb-bank.az/az/ferdi/kreditler/ipoteka-kreditleri\"]")
    private WebElement mortgage;

    public MortgagePage chooseMortgage(){
        mortgage.click();
        return new MortgagePage(driver);
    }

}

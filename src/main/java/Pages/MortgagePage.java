package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MortgagePage extends AbsBasePage{
    public MortgagePage(WebDriver driver) {
        super(driver);
    }
    @FindBy(css = "div.hover>div>div>div>div>a[href='https://abb-bank.az/az/ferdi/kreditler/daxili-ipoteka-krediti#applyForm']")
    private WebElement internalMortgage;

    public InternalMortgagePage internalMortgagePage(){
        internalMortgage.click();
        return new InternalMortgagePage(driver);
    }
}

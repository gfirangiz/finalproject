package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CardsPage extends AbsBasePage{

    public CardsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy (css = "div.container>div>div>div>div>a[href='https://abb-bank.az/az/ferdi/kartlar/tamkart-debet-kartlari/tamkart-mastercard-standard-paypass-debet']")
    private WebElement selectedCard;

    public SelectedCardPage selectedCardPage(){
        selectedCard.click();
        return new SelectedCardPage(driver);
    }
}

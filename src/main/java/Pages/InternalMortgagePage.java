package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class InternalMortgagePage extends AbsBasePage{
    public InternalMortgagePage(WebDriver driver) {
        super(driver);
    }
    @FindBy(css = "label[for='from_abb_no']")
    public WebElement formNoButton;

    @FindBy(css = "input#from_abb_no")
    private WebElement formNoRadio;

    public void checkRadioButton(boolean selected){
        Assert.assertEquals(selected,
                  formNoRadio.isSelected());
    }
}

package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Bank24_7_Page extends AbsBasePage{
    public Bank24_7_Page(WebDriver driver) {
        super(driver);
    }
    @FindBy(css = "span>a[href='https://ibar.az/az/ferdi/bank-24-7/iba-mobile']")
    private WebElement abbMobileBank;

    public AbbMobileBankPage abbMobileBankPage(){
        abbMobileBank.click();
        return new AbbMobileBankPage(driver);
    }
}
